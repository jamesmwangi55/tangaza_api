var express = require('express');
var logger = require('morgan');
var bodyParser = require('body-parser');
var cors = require('cors');
var helmet = require('helmet');
require('dotenv').load();
var port = process.env.PORT || 3030

console.log(port);

var nairobiwire_job = require('./blogs/nairobi_wire/nairobiwire_job')
var nation_job = require('./blogs/nation/nation_job')
// var standard_job = require('./lib/standard_job')
// var hapakenya_job = require('./lib/hapakenya_job')
var cleandb = require('./lib/cleandb');

nairobiwire_job;
nation_job;
// standard_job;
// hapakenya_job;
cleandb;


var posts = require('./routes/posts');
var nairobiwire = require('./blogs/nairobi_wire/nairobiwireRoutes');
var nation = require('./blogs/nation/nationRoutes');
var standard = require('./routes/standard');
var football = require('./routes/football');
var showbiz = require('./routes/showbiz');
var hapakenya = require('./routes/hapakenya.js')

var app = express();

app.use(logger('dev'));
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());
app.use(cors());
app.use(helmet());

app.use('/tangaza/', express.static('public'))
app.use('/build', express.static('build'))
app.use('/scripts', express.static(__dirname + '/node_modules/bootstrap/dist/'));

app.use('/tangaza/posts', posts);
// app.use('/nairobiwire', nairobiwire);
// app.use('/nation', nation);
// app.use('/standard', standard);
// app.use('/football', football);
// app.use('/showbiz', showbiz);

app.use(function(error, request, response, next){
  response.status(error.status || 500);
  response.json({error: error.message});
});

app.listen(port, function(){
  console.log('App is listening on: ' + port);
});
