'use strict'
var CronJob = require('cron').CronJob;
var request = require('request');
var sync_request = require('sync-request');
var YQL = require('yql');
var query = new YQL('SHOW TABLES');
var logger = require('../../util/logger');
var Promise = require('bluebird');
var cheerio = require('cheerio');
var then_request = require('then-request');

query.exec(function (error, response) {
    // Do something with results (response.query.results)
});

var getImage = function (imageLink) {
    return new Promise(function (resolve, reject) {
        then_request('GET', imageLink).done(function (res) {
            var $ = cheerio.load(res.getBody().toString());
            var img = $('div img.aligncenter').first().attr('src');
            var article = $('.theiaPostSlider_slides div');
            if (img !== undefined && article !== undefined) {
                //console.log($('div img.photo_article').first().attr('src'));

                var data = {};
                data.img = img;
                data.article = article;
                resolve(data);
            } else {
                return 0;
            }
        });
    });
}

module.exports.cronJob = new CronJob('* * * * *', function () {
    var nairobiwireQuery = new YQL('select * from atom where url="http://nairobiwire.com/feed/atom/"');
    nairobiwireQuery.exec(function (error, response) {
        // Do something with results (response.query.results)
        //console.log(response.query.results.entry);
        if (response !== undefined) {

            if (response.query.results != null) {
                for (var i = 0; i < response.query.results.entry.length; i++) {
                    (function (i) {
                        //console.log(response.query.results.entry[i].title.content);

                        getImage(response.query.results.entry[i].content.base).then(function (data) {
                            var theImage = data.img
                            var article = data.article
                            // console.log(theImage);
                            //console.log(''+article);
                            if (typeof response.query.results.entry[i] === 'undefined') {
                                logger.info('nairobiwire', 'article does not exist');
                            } else {
                                var postData = {
                                    id: response.query.results.entry[i].id,
                                    title: response.query.results.entry[i].title.content,
                                    content: response.query.results.entry[i].summary.content,
                                    link: response.query.results.entry[i].content.base,
                                    upvotes: 0,
                                    downvotes: 0,
                                    source: response.query.results.entry[i].category[0].scheme,
                                    published: new Date(),
                                    blog: "nairobiwire.com",
                                    img: theImage,
                                    fullContent: response.query.results.entry[i].content.content,
                                    article: '' + article
                                }

                                // send each of the items to the database
                                request({
                                    url: 'http://localhost:' + 3030 + '/tangaza/posts', //URL to hit
                                    method: 'POST',
                                    //vars post the following key/values as form
                                    json: postData
                                }, function (error, response, body) {
                                    if (error) {
                                        logger.info(error);
                                    } else {
                                        logger.info(response.statusCode, ' nairobiwire');
                                    }
                                });

                            }

                        })



                    })(i);
                }
            }

        } else {
            logger.info("nairobiwire Response is undefined")
        }


    });

    // +===========================================================================================
    // end of nairobiwire feed
    // ===========================================================================================

}, null, true, 'America/Los_Angeles');
