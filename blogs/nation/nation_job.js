'use strict'
var CronJob = require('cron').CronJob;
var request = require('request');
var YQL = require('yql');
var query = new YQL('SHOW TABLES');
var cheerio = require('cheerio');
var logger = require('../../util/logger');
var shortid = require('shortid');
var then_request = require('then-request');

query.exec(function (error, response) {
    // Do something with results (response.query.results)
});


var getImage = function (imageLink) {
    return new Promise(function (resolve, reject) {
        then_request('GET', imageLink).done(function (res) {
            var $ = cheerio.load(res.getBody().toString());
            var img = $('article div img');
            var article = $('section.body-copy div p').not('.story-information, .email-print-holder, .noprint p');
            if (img !== undefined && article !== undefined) {
                var data = {};
                data.image = img.first().attr('src')
                data.article = article;
                resolve(data);
            } else {
                return 0;
            }
        });
    });
}

// var getArticle = function(link){
//     return new Promise(function (resolve, reject){

//     })
// }


//'00,30 * * * *' every 30 minutes

module.exports.cronJob = new CronJob('00,30 * * * *', function () {

    // acquire nation rss feed
    var nationQuery = new YQL('select * from rss where url="http://www.nation.co.ke/latestrss.rss"');
    nationQuery.exec(function (error, response) {
        // Do something with results (response.query.results)
        //console.log(response.query.results.entry);
        console.log("Outsite Nation Job")
        if (response != undefined) {
            if (response.query.results != null) {
                for (var i = 0; i < response.query.results.item.length; i++) {
                    (function (i) {

                        getImage(response.query.results.item[i].link).then(function (data) {

                            var fullImageLink = "http://www.nation.co.ke" + data.image;
                            var article = data.article.toString();

                            var postData = {
                                id: response.query.results.item[i].link.substring(0, 100),
                                title: response.query.results.item[i].title,
                                content: response.query.results.item[i].description,
                                link: response.query.results.item[i].link,
                                upvotes: 0,
                                downvotes: 0,
                                source: 'www.nation.co.ke',
                                published: new Date(),
                                blog: "nation.co.ke",
                                img: fullImageLink,
                                fullContent: response.query.results.item[i].description,
                                article: article
                            }

                            // send each of the items to the database
                            request({
                                url: 'http://localhost:' + 3030 + '/tangaza/posts', //URL to hit
                                method: 'POST',
                                //vars post the following key/values as form
                                json: postData
                            }, function (error, response, body) {
                                if (error) {
                                    logger.info("Nation" + error);
                                } else {
                                    logger.info(response.statusCode, ' nation');
                                }
                            });

                        })


                    })(i);
                }
            }
        }


    });


}, null, true, 'America/Los_Angeles');
