var path = require('path');
var bootstrapPath = path.join(
    __dirname,
    'node_modules/bootstrap/dist/css'
);




module.exports = {
  // the main entry of our app
  entry: ['./src/index.js'],
  // output configuration
  output: {
    path: __dirname + '/build/',
    publicPath: 'build/',
    filename: 'build.js'
  },

  resolve: {
    alias: {
        jquery: path.join(__dirname, 'node_modules/jquery/dist')
    },
    extensions: ['', '.js', '.css'],
    modulesDirectories: ['node_modules', bootstrapPath]
  },

  module: {
    loaders: [
      // process *.vue files using vue-loader
      { test: /\.vue$/, loader: 'vue' },
      // process *.js files using babel-loader
      // the exclude pattern is important so that we don't
      // apply babel transform to all the dependencies!
      { test: /\.js$/, loader: 'babel', exclude: /node_modules/ },

      { test: /\.css$/, loader: "style-loader!css-loader" }
    ]
  },

  babel: {
    presets: ['es2015'],
    plugins: ['transform-runtime']
  }
}
