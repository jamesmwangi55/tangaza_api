'use strict'
var CronJob = require('cron').CronJob;
var request = require('request');
var YQL = require('yql');
var query = new YQL('SHOW TABLES');
var logger = require('../util/logger');
var shortid = require('shortid');
query.exec(function (error, response) {
	// Do something with results (response.query.results)
});

module.exports.cronJob = new CronJob('*/1 * * * *', function(){

                // acquire world rss feed
                var worldQuery = new YQL('select * from rss where url="http://www.dailymail.co.uk/news/index.rss"');
                worldQuery.exec(function (error, response) {
              	// Do something with results (response.query.results)
                //console.log(response.query.results.entry);
                for(var i = 0; i < response.query.results.item.length; i++)
                  {
                    //console.log(response.query.results.entry[i].title.content);

                    var postData = {
                            id: response.query.results.item[i].link.substring(0, 100),
                            title: response.query.results.item[i].title,
                            content: response.query.results.item[i].description[0],
                            link: response.query.results.item[i].link,
                            upvotes: 0,
                            downvotes: 0,
                            source: 'http://www.dailymail.co.uk/news/',
                            published: new Date(),
                            blog: "dailymail.co.uk",
                            img: response.query.results.item[i].content.url,
                            fullContent: response.query.results.item[i].description[0]
                          }

                    // send each of the items to the database
                    request({
                            url: 'http://localhost:'+ 3030 + '/tangaza/posts', //URL to hit
                            method: 'POST',
                            //vars post the following key/values as form
                            json: postData
                        }, function(error, response, body){
                            if(error) {
                                logger.info(error);
                            } else {
                                logger.info(response.statusCode, ' world');
                        }
                        });
        }
    });


}, null, true, 'America/Los_Angeles');
