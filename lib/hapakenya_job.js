'use strict'
var CronJob = require('cron').CronJob;
var request = require('request');
var sync_request = require('sync-request');
var YQL = require('yql');
var query = new YQL('SHOW TABLES');
var logger = require('../util/logger');
var Promise = require('bluebird');
var cheerio = require('cheerio');
var then_request = require('then-request');
var ArticleParser = require('article-parser');
var extractor = require('article-extractor');
query.exec(function (error, response) {
    // Do something with results (response.query.results)
});

var getImage = function (imageLink) {
    return new Promise(function (resolve, reject) {
        then_request('GET', imageLink).done(function (res) {
            var $ = cheerio.load(res.getBody().toString());
            var img = $('div img.entry-thumb');
            console.log(img.first().attr('src'));
            if (img !== undefined) {
                //console.log($('div img.photo_article').first().attr('src'));
                resolve($('div img.entry-thumb').first().attr('src'));
            } else {
                return 0;
            }
        });
    });
}

module.exports.cronJob = new CronJob('*/5 * * * *', function () {
    var hapakenyaQuery = new YQL('select * from atom where url="http://www.hapakenya.com/feed/atom/"');
    hapakenyaQuery.exec(function (error, response) {
        // Do something with results (response.query.results)
        //console.log(response.query.results.entry);
        if (response !== undefined) {
            if (response.query.results != null) {
                for (var i = 0; i < response.query.results.entry.length; i++) {
                    (function (i) {
                        getImage(response.query.results.entry[i].id).then(function (imageLink) {
                            var theImage = imageLink
                            if (typeof response.query.results.entry[i] === 'undefined') {
                                logger.info('hapakenya', 'article does not exist');
                            } else {
                                if (theImage !== undefined) {
                                    saveToDatabase(theImage)

                                } else {
                                    theImage = 0;
                                    saveToDatabase(theImage)
                                }

                            }

                            function saveToDatabase(image) {
                                var postData = {
                                    id: response.query.results.entry[i].id,
                                    title: response.query.results.entry[i].title.content,
                                    content: response.query.results.entry[i].summary.content,
                                    link: response.query.results.entry[i].id,
                                    upvotes: 0,
                                    downvotes: 0,
                                    source: response.query.results.entry[i].category[0].scheme,
                                    published: new Date(),
                                    blog: "hapakenya.com",
                                    img: image,
                                    fullContent: response.query.results.entry[i].summary.content,
                                    article: ""
                                }

                                // send each of the items to the database
                                request({
                                    url: 'http://localhost:' + 3030 + '/tangaza/posts', //URL to hit
                                    method: 'POST',
                                    //vars post the following key/values as form
                                    json: postData
                                }, function (error, response, body) {
                                    if (error) {
                                        logger.info("Hapa kenya " + error);
                                    } else {
                                         logger.info(response.statusCode, ' hapakenya');
                                    }
                                });

                            }

                        })

                    })(i);
                }
            }


        } else {
            logger.info("hapakenya Response is undefined")
        }


    });

    // +===========================================================================================
    // end of hapakenya feed
    // ===========================================================================================

}, null, true, 'America/Los_Angeles');
