'use strict'
var CronJob = require('cron').CronJob;
var request = require('request');
var YQL = require('yql');
var query = new YQL('SHOW TABLES');
var logger = require('../util/logger');
query.exec(function (error, response) {
	// Do something with results (response.query.results)
});

module.exports.cronJob = new CronJob('*/1 * * * *', function(){


    // acquire nairobiwire atom feed
    var query = new YQL('select * from atom where url="http://nairobiwire.com/feed/atom"');
    query.exec(function (error, response) {
    	// Do something with results (response.query.results)
      //console.log(response.query.results.entry);
      for(var i = 0; i < response.query.results.entry.length; i++)
        {
          //console.log(response.query.results.entry[i].title.content);

          var postData = {
                  id: response.query.results.entry[i].id,
                  title: response.query.results.entry[i].title.content,
                  content: response.query.results.entry[i].summary.content,
                  link: response.query.results.entry[i].content.base,
                  upvotes: 0,
                  downvotes: 0,
                  source: response.query.results.entry[i].category[0].scheme,
                  published: new Date(),
                  blog: "nairobiwire.com"
                }

          // send each of the items to the database
          request({
                  url: 'http://localhost:'+ 3030 + '/tangaza/posts', //URL to hit
                  method: 'POST',
                  //vars post the following key/values as form
                  json: postData
              }, function(error, response, body){
                  if(error) {
                      //console.log(error);
                      logger.info(error);
                  } else {
                      logger.info(response.statusCode, ' nairobiwire');
                      
              }
              });

            // send to the nairobi wire table
          request({
                  url: 'http://localhost:'+ 3030 + '/nairobiwire', //URL to hit
                  method: 'POST',
                  //vars post the following key/values as form
                  json: postData
              }, function(error, response, body){
                  if(error) {
                      //console.log(error);
                      logger.info(error);
                  } else {
                      logger.info(response.statusCode, ' nairobiwire');
                      
              }
              });  



            }

          });


                // acquire ghafla rss feed
                var ghaflaQuery = new YQL('select * from rss where url="http://www.ghafla.co.ke/component/ninjarsssyndicator/?feed_id=1&format=raw"');
                ghaflaQuery.exec(function (error, response) {
              	// Do something with results (response.query.results)
                //console.log(response.query.results.entry);
                for(var i = 0; i < response.query.results.item.length; i++)
                  {
                    //console.log(response.query.results.entry[i].title.content);

                    var postData = {
                            id: response.query.results.item[i].link.substring(0, 126),
                            title: response.query.results.item[i].title,
                            content: response.query.results.item[i].description,
                            link: response.query.results.item[i].link,
                            upvotes: 0,
                            downvotes: 0,
                            source: 'http://www.ghafla.co.ke/',
                            published: new Date(),
                            blog: "ghafla.co.ke"
                          }

                    // send each of the items to the database
                    request({
                            url: 'http://localhost:'+ 3030 + '/posts', //URL to hit
                            method: 'POST',
                            //vars post the following key/values as form
                            json: postData
                        }, function(error, response, body){
                            if(error) {
                                logger.info(error);
                            } else {
                                logger.info(response.statusCode, ' ghafla');
                        }
                        });

                     // send each of the items to the ghafla table
                    request({
                            url: 'http://localhost:'+ 3030 + '/ghafla', //URL to hit
                            method: 'POST',
                            //vars post the following key/values as form
                            json: postData
                        }, function(error, response, body){
                            if(error) {
                                logger.info(error);
                            } else {
                                logger.info(response.statusCode, ' ghafla');
                        }
                        });

        }
    });



    var niageQuery = new YQL('select * from atom where url="http://niaje.com/feed/atom/"');
    niageQuery.exec(function (error, response) {
      // Do something with results (response.query.results)
      //console.log(response.query.results.entry);
      for(var i = 0; i < response.query.results.entry.length; i++)
        {
          //console.log(response.query.results.entry[i].title.content);

          var postData = {
                  id: response.query.results.entry[i].id,
                  title: response.query.results.entry[i].title.content,
                  content: response.query.results.entry[i].summary.content,
                  link: response.query.results.entry[i].content.base,
                  upvotes: 0,
                  downvotes: 0,
                  source: response.query.results.entry[i].category[0].scheme,
                  published: new Date(),
                  blog: "niaje.com"
                }

          // send each of the items to the database
          request({
                  url: 'http://localhost:'+ 3030 + '/posts', //URL to hit
                  method: 'POST',
                  //vars post the following key/values as form
                  json: postData
              }, function(error, response, body){
                  if(error) {
                      logger.info(error);
                  } else {
                      logger.info(response.statusCode, ' niage');
              }
              });

          // send each of the items to the niaje database
          request({
                  url: 'http://localhost:'+ 3030 + '/niaje', //URL to hit
                  method: 'POST',
                  //vars post the following key/values as form
                  json: postData
              }, function(error, response, body){
                  if(error) {
                      logger.info(error);
                  } else {
                      logger.info(response.statusCode, ' niage');
              }
              });
            }

          });

    // +===========================================================================================
    // end of niaje feed
    // ===========================================================================================

}, null, true, 'America/Los_Angeles');
