'use strict'
var CronJob = require('cron').CronJob;
var request = require('request');
var then_request = require('then-request');
var YQL = require('yql');
var query = new YQL('SHOW TABLES');
var logger = require('../util/logger');
var cheerio = require('cheerio');
var Promise = require('bluebird');
var shortid = require('shortid');
query.exec(function(error, response) {
    // Do something with results (response.query.results)
});

var getImage = function(imageLink) {
    return new Promise(function(resolve, reject) {
        then_request('GET', imageLink).done(function(res) {
            var $ = cheerio.load(res.getBody().toString());
            var img = $('p img.attachment-post-full');
            if (img !== undefined) {
                //console.log($('div img.photo_article').first().attr('src'));
                resolve($('div img.attachment-post-full').first().attr('src'));
            } else {
                return 0;
            }
        });
    });
}

module.exports.cronJob = new CronJob('*/1 * * * *', function() {
    var nairobinewsQuery = new YQL('select * from rss where url="http://nairobinews.nation.co.ke/feed/"');
    nairobinewsQuery.exec(function(error, response) {
        // Do something with results (response.query.results)
        //console.log(response.query.results.entry);
        if (response !== undefined && response !== null) {
            for (var i = 0; i < response.query.results.item.length; i++) {
                (function(i) {

                    getImage(response.query.results.item[i].link).then(function(imageLink) {
                        var theImage = imageLink
                        if (typeof response.query.results.item[i] === 'undefined') {
                            logger.info('nairobinews', 'article does not exist');
                        } else {
                            if (theImage !== undefined) {
                                saveToDatabase(theImage)
                            } else {
                                theImage = 0;
                                saveToDatabase(theImage)
                            }

                        }

                    })

                    function saveToDatabase(image) {
                        var postData = {
                            id: response.query.results.item[i].link.substring(0, 100),
                            title: response.query.results.item[i].title,
                            content: response.query.results.item[i].description,
                            link: response.query.results.item[i].link,
                            upvotes: 0,
                            downvotes: 0,
                            source: "http://nairobinews.nation.co.ke/",
                            published: new Date(),
                            blog: "nairobinews.nation.co.ke",
                            img: image,
                            fullContent: response.query.results.item[i].encoded
                        }

                        // send each of the items to the database
                        request({
                            url: 'http://localhost:' + 3030 + '/tangaza/posts', //URL to hit
                            method: 'POST',
                            //vars post the following key/values as form
                            json: postData
                        }, function(error, response, body) {
                            if (error) {
                                logger.info(error);
                            } else {
                          //      logger.info(response.statusCode, response.statusMessage, ': nairobinews');
                            }
                        });
                    }

                })(i);
            }

        } else {
            logger.info("Niaje Response is undefined")
        }


    });

    // +===========================================================================================
    // end of niaje feed
    // ===========================================================================================

}, null, true, 'America/Los_Angeles');
