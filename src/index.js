import Vue from 'vue'
import App from './components/App.vue'
import Home from './components/Home.vue'
import Ghafla from './components/Ghafla.vue'
import Nairobiwire from './components/Nairobiwire.vue'
import Niaje from './components/Niaje.vue'
import VueRouter from 'vue-router'
import VueResource from 'vue-resource'
Vue.use(VueResource)
Vue.use(VueRouter)

export var router = new VueRouter()

Vue.config.debug = true

// Set up routing and match routes to components
router.map({
  '/': {
    component: Home
  },
  '/ghafla': {
    component: Ghafla
  },
  '/nairobiwire': {
    component: Nairobiwire
  },
  '/niaje': {
    component: Niaje
  }
})

// Redirect to the home route if any routes are unmatched
router.redirect({
  '*': '/'
})

// Start the app on the #app div
router.start(App, '#app')