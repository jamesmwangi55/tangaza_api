// tell vue to use Vue router
Vue.use(VueRouter);

// Intialize the router with operations
var router = new VueRouter({
  history: false
});

// Redirect certain routes to toher routes
// router.redirect({

// })

// define some components
// var All = Vue.extend({
//   template: 
// })



new Vue({
  el: '#app',
  data: {
      posts: []
  },

  ready: function(){
    //this.fetchPosts();
    setInterval( this.fetchPosts(), 2*60*1000);

  },

  methods: {
      fetchPosts: function(){
        this.$http.get('/posts/1').then(function(posts) {
          console.log(posts.data.length);
        //  var parsedData = JSON.parse(posts.data);

          this.$set('posts', posts.data);
          }).catch(function(error) {
            console.log(error);
          });

      },
      loadPrevious: function(){

      },
      loadNext: function(){

      }
  }

});
