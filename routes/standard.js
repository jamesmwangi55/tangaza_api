var express = require('express');
var rdb = require('../lib/rethink');
var router = express.Router();

router.get('/:page', function (request, response) {

    var pageNum = parseInt(request.params.page) || 1;

    rdb.findAll('standard', 'published', (pageNum-1)*30, pageNum * 30)
    .then(function (standard) {
        response.json(standard);
    });
});

router.get('/:blog/:page', function (request, response) {

    var pageNum = parseInt(request.params.page) || 1;

    rdb.findAll(request.params.blog, 'published', (pageNum-1)*30, pageNum * 30)
    .then(function (standard) {
        response.json(standard);
    });
});

router.get('/:id', function (request, response, next) {
    rdb.find('standard', request.params.id)
    .then(function (post) {
        if(!post) {
            var notFoundError = new Error('Post not found');
            notFoundError.status = 404;
            return next(notFoundError);
        }

        response.json(post);
    });
});

router.post('/',  function (request, response) {

        var newPost = {
            id: request.body.id,
            title: request.body.title,
            content: request.body.content,
            link: request.body.link,
            upvotes: 0,
            downvotes: 0,
            source: request.body.source,
            published: request.body.published,
            views: 0,
            blog: request.body.blog,
            img: request.body.img,
            fullContent: request.body.fullContent
        };

        rdb.save('standard', newPost)
        .then(function (result) {
            response.json(result);
          });
});

router.put('/:id', function (request, response) {
    rdb.find('standard', request.params.id)
    .then(function (post) {
        var updatePost = {
          title: request.body.title || post.title,
          content: request.body.content || post.content,
          link: request.body.link || post.link,
          upvotes: request.body.upvotes || post.upvotes,
          downvotes: request.body.downvotes || post.downvotes
        };

        rdb.edit('post', post.id, updatePost)
        .then(function (results) {
            response.json(results);
        });
    });
});

router.delete('/:id', function (request, response) {
    rdb.destroy('standard', request.params.id)
    .then(function (results) {
        response.json(results);
    });
});

module.exports = router;
